package com.haris.equipment.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

/**
 * @author Tamas
 */

@SpringBootApplication
@EnableR2dbcRepositories(basePackages = "com.haris.equipment.manager.equipments")
public class EquipmentManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EquipmentManagerApplication.class, args);
    }

}
