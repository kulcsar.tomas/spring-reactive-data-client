package com.haris.equipment.manager.errorhandling;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

/**
 * @author Tamas
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

    private ZonedDateTime zonedDateTime;
    private String message;
    private String cause;
    private int status;

}
