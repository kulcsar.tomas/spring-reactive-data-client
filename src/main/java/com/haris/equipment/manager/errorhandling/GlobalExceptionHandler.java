package com.haris.equipment.manager.errorhandling;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import javax.xml.bind.ValidationException;
import java.time.ZonedDateTime;

/**
 * @author Tamas
 */

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler
    public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {

        HttpHeaders headers = new HttpHeaders();

        if (ex instanceof DataAccessException) {
            if (ex.getCause() instanceof DataIntegrityViolationException) {
                return handleExceptionInternal(ex, ErrorMessage.DATA_INTEGRITY_VIOLATION,
                        HttpStatus.CONFLICT, headers, request);

            }
            return handleExceptionInternal( ex, ErrorMessage.DATA_ACCESS_EXCEPTION,
                    HttpStatus.CONFLICT, headers, request);

        } else if (ex instanceof ValidationException) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                return handleExceptionInternal(ex, ErrorMessage.CONSTRAINT_VIOLATION,
                        HttpStatus.CONFLICT, headers, request);

            }
            return handleExceptionInternal(ex, ErrorMessage.VALIDATION_EXCEPTION,
                    HttpStatus.CONFLICT, headers, request);

        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            return handleExceptionInternal(ex, ex.getMessage(), HttpStatus.BAD_REQUEST, headers, request);

        } else {
            return handleExceptionInternal(ex, ErrorMessage.REQUEST_PROCESSING_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR, headers, request);
        }
    }

    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, ErrorMessage message, HttpStatus status,
                                                               HttpHeaders headers,
                                                               WebRequest request) {
        return handleExceptionInternal(ex, message.getMessage(), status, headers, request);
    }

    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, String message, HttpStatus status,
                                                               HttpHeaders headers,
                                                               WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);
        }

        String cause = ex.getCause() == null ? null : ex.getCause().getMessage();

        log.error(ex.toString());
        return new ResponseEntity<>(
                    new ApiError(ZonedDateTime.now(), message, cause, status.value()),
                    headers, status
                );
    }

}
