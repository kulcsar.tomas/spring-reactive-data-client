package com.haris.equipment.manager.errorhandling;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author Tamas
 */

@Getter
public class CustomException extends Exception {

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public CustomException(String errorMessage) {
        super(errorMessage);
    }

    public CustomException(ErrorMessage errorMessage, HttpStatus httpStatus) {
        super(errorMessage.getMessage());
        this.httpStatus = httpStatus;
    }

}
