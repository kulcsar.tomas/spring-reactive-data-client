package com.haris.equipment.manager.equipments;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * @author Tamas
 */

@Data
@MappedSuperclass
public abstract class Equipment {

    @org.springframework.data.annotation.Id
    @Column(name = "id")
    UUID id;

    public String getIdString() {
        if (id != null) {
            return id.toString();
        }
        return "";
    }
}
