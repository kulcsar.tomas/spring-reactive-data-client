package com.haris.equipment.manager.equipments.machine;

import com.haris.equipment.manager.equipments.machine.models.MachineDTO;
import com.haris.equipment.manager.errorhandling.CustomException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * @author Tamas
 */

@RestController
@RequestMapping(path = "/machines", produces = "application/json")
public class MachineReactiveRestController {

    MachineReactiveService machineReactiveService;

    @Autowired
    public MachineReactiveRestController(MachineReactiveService machineReactiveService) {
        this.machineReactiveService = machineReactiveService;
    }

    @Operation(summary = "Fetch all machines", description = "NEW DESC")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Machines found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Machines not found",
                    content = @Content)
    })
    @GetMapping
    public Flux<MachineDTO> getAllMachines(){
        return machineReactiveService.getMachineDTOs();
    }

    @Operation(summary = "Fetch machine by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Machine found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Machine not found",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Data error",
                    content = @Content),
    })
    @GetMapping("/{id}")
    public Mono<ResponseEntity<MachineDTO>> getMachineById(@PathVariable String id){
        Mono<MachineDTO> machine = machineReactiveService.getMachineDTOById(id);
        return machine.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Save machine")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Machine saved",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Data error",
                    content = @Content),
    })
    @PostMapping
    public Mono<ResponseEntity<MachineDTO>> createMachine(@RequestBody MachineDTO machine) throws CustomException {
        return machineReactiveService.createMachine(machine).map(machineDTO -> new ResponseEntity<>(machineDTO, HttpStatus.CREATED));
    }

    @Operation(summary = "Update machine")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Machine updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MachineDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Data error",
                    content = @Content),
    })
    @PutMapping
    public Mono<ResponseEntity<MachineDTO>> updateMachineById(@RequestBody MachineDTO machine){
        return machineReactiveService.updateMachine(machine)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    /** optional route should be used with proper authorization **/
    @Operation(summary = "Update machine")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Machine updated"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Data error",
                    content = @Content),
    })
    @DeleteMapping(value = {"/{id}", "/{id}/hard-delete/{hardDeleteEnabled}"})
    public Mono<ResponseEntity<Void>> deleteMachineById(@PathVariable String id,
                                                        @PathVariable(required = false) Optional<Boolean> hardDeleteEnabled){
        return machineReactiveService.deleteMachine(id, hardDeleteEnabled)
                .map( r -> ResponseEntity.ok().<Void>build());
    }

}
