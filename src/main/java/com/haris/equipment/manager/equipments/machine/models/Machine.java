package com.haris.equipment.manager.equipments.machine.models;

import com.haris.equipment.manager.equipments.Equipment;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * @author Tamas
 */

@Entity(name = "Machine")
@Table(name = "machine")
@Data
@EqualsAndHashCode(callSuper = true)
public class Machine extends Equipment {

    @Column(name = "name")
    private String name;
    @Column(name = "created_at")
    private ZonedDateTime createdAt;
    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;
    @Column(name = "deleted_at")
    private ZonedDateTime deletedAt;
    @Column(name = "delete_flag")
    private boolean deleteFlag;

}
