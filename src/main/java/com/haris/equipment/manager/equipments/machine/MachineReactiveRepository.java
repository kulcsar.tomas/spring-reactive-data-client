package com.haris.equipment.manager.equipments.machine;

import com.haris.equipment.manager.equipments.machine.models.Machine;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author Tamas
 */

@Repository()
public interface MachineReactiveRepository extends R2dbcRepository<Machine, UUID> {

    Flux<Machine> findAllByDeleteFlagFalseOrderByUpdatedAtDesc();

    Mono<Machine> findById(String id);

    @Query("update machine set delete_flag=true, deleted_at=now() where id=:id")
    @Modifying
    Mono<Void> softDelete(UUID id);
}
