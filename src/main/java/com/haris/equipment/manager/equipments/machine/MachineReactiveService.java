package com.haris.equipment.manager.equipments.machine;

import com.haris.equipment.manager.equipments.machine.models.Machine;
import com.haris.equipment.manager.equipments.machine.models.MachineDTO;
import com.haris.equipment.manager.errorhandling.CustomException;
import com.haris.equipment.manager.errorhandling.ErrorMessage;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Tamas
 */

@Service
@Transactional(rollbackOn = Exception.class)
public class MachineReactiveService {

    private final MachineReactiveRepository machineReactiveRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public MachineReactiveService(final MachineReactiveRepository machineReactiveRepository, final ModelMapper modelMapper) {
        this.machineReactiveRepository = machineReactiveRepository;
        this.modelMapper = modelMapper;
    }

    @PostConstruct
    public void addMappings() {
        Converter<String, UUID> getUUID = context ->
                context.getSource() == null ? null : UUID.fromString(context.getSource());

        this.modelMapper.addMappings(new PropertyMap<Machine, MachineDTO>() {
            @Override
            protected void configure() {
                map(source.getIdString(), destination.getId());
            }
        });
        this.modelMapper.addMappings(new PropertyMap<MachineDTO, Machine>() {
            @Override
            protected void configure() {
                using(getUUID).map(source.getId(), destination.getId());
            }
        });
    }

    public Flux<MachineDTO> getMachineDTOs() {
        return this.getMachines().map(this::convertToDto);
    }

    public Flux<Machine> getMachines() {
        return this.machineReactiveRepository.findAllByDeleteFlagFalseOrderByUpdatedAtDesc();
    }

    public Mono<MachineDTO> getMachineDTOById(String uuid) {
        return this.getMachineById(uuid).map(this::convertToDto);
    }

    public Mono<Machine> getMachineById(String uuid) {
        return this.machineReactiveRepository.findById(uuid);
    }

    public Mono<MachineDTO> createMachine(MachineDTO machine) throws CustomException {
        return this.createMachine(convertToEntity(machine)).map(this::convertToDto);
    }

    public Mono<Machine> createMachine(Machine machine) throws CustomException {
        if (machine.getId() == null) {
            return this.machineReactiveRepository.save(machine);
        }
        throw new CustomException(ErrorMessage.ID_PROVIDED_ON_CREATION, HttpStatus.BAD_REQUEST);
    }

    public Mono<MachineDTO> updateMachine(MachineDTO machine) {
        return this.updateMachine(convertToEntity(machine)).map(this::convertToDto);
    }

    public Mono<Machine> updateMachine(Machine machine) {
        return machineReactiveRepository.findById(machine.getId())
                .flatMap(existingMachine -> {
                    machine.setUpdatedAt(ZonedDateTime.now());
                    if (machine.getCreatedAt() == null) {
                        machine.setCreatedAt(existingMachine.getCreatedAt());
                    }
                    return machineReactiveRepository.save(machine);
                });
    }

    public Mono<Void> deleteMachine(String uuid, Optional<Boolean> hardDeleteEnabled) {
        UUID id = UUID.fromString(uuid);
        if (hardDeleteEnabled.isPresent() && hardDeleteEnabled.get()) {
            return machineReactiveRepository.deleteById(id);
        } else {
            return machineReactiveRepository.softDelete(id);
        }
    }

    private MachineDTO convertToDto(Machine machine) {
        return this.modelMapper.map(machine, MachineDTO.class);
    }

    private Machine convertToEntity(MachineDTO machineDto) {
        return this.modelMapper.map(machineDto, Machine.class);
    }
}
