package com.haris.equipment.manager.equipments.machine.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.ZonedDateTime;

/**
 * @author Tamas
 */

@Data
public class MachineDTO {

    private String id;
    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Schema(name = "created_at")
    private ZonedDateTime createdAt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Schema(name = "updated_at")
    private ZonedDateTime updatedAt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Schema(name = "deleted_at")
    private ZonedDateTime deletedAt;

}
