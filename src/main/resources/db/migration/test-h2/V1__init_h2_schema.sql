
CREATE TABLE machine (
  id         uuid DEFAULT random_uuid(),
  name       varchar(50) NOT NULL,
  created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP(),
  updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP(),
  deleted_at timestamp with time zone DEFAULT NULL,
  delete_flag boolean DEFAULT false,
  CONSTRAINT id_machine PRIMARY KEY ( id )
);
