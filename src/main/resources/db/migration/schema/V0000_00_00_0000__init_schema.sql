
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE machine (
  id         uuid DEFAULT uuid_generate_v4(),
  name       varchar(50) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
  delete_flag boolean DEFAULT false,
  CONSTRAINT id_machine PRIMARY KEY ( id )
);
