package com.haris.equipment.manager.equipment.machine;

import com.haris.equipment.manager.equipments.machine.*;
import com.haris.equipment.manager.equipments.machine.models.Machine;
import com.haris.equipment.manager.equipments.machine.models.MachineDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.internal.verification.VerificationModeFactory.times;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = MachineReactiveRestController.class)
@Import({MachineReactiveService.class, ModelMapper.class})
@ActiveProfiles("test")
public class MachineReactiveRestControllerTest {

    Logger logger = LoggerFactory.getLogger(MachineReactiveRestControllerTest.class);

    private static final String ENDPOINT = "/machines";
    private static final String INITIALIZED_NAME = "MachineReactiveRestControllerTest";

    @MockBean
    private MachineReactiveRepository reactiveRepository;

    @Autowired
    private WebTestClient webTestClient;
    private Machine machineEntity;
    private MachineDTO machineDTO;


    @BeforeEach
    void initMachineData() {
        this.machineDTO = new MachineDTO();
        this.machineEntity = new Machine();
        machineDTO.setName(INITIALIZED_NAME);
        machineEntity.setName(INITIALIZED_NAME);
    }

    @AfterEach
    public void deconstruct() {
        this.machineDTO = null;
        this.machineEntity = null;
    }

    @Test
    void postMachineTest() {
        Mockito.when(this.reactiveRepository.save(this.machineEntity)).thenReturn(Mono.just(this.machineEntity));

        webTestClient.post()
            .uri(ENDPOINT)
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(this.machineDTO))
            .exchange()
            .expectStatus()
            .isCreated();

        Mockito.verify(reactiveRepository, times(1)).save(this.machineEntity);
    }

    @Test
    void getMachinesTest() {

        List<Machine> list = new ArrayList<>();
        list.add(this.machineEntity);

        Flux<Machine> machineDTOFlux = Flux.fromIterable(list);

        Mockito.when(reactiveRepository.findAllByDeleteFlagFalseOrderByUpdatedAtDesc())
                .thenReturn(machineDTOFlux);

        webTestClient.get().uri(ENDPOINT)
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(MachineDTO.class);

        Mockito.verify(reactiveRepository, times(1)).findAllByDeleteFlagFalseOrderByUpdatedAtDesc();
    }

    @Test
    void getMachineByIdTest()
    {
        UUID uuid = UUID.randomUUID();
        this.machineEntity.setId(uuid);
        this.machineEntity.setCreatedAt(ZonedDateTime.now());

        Mockito.when(reactiveRepository.findById(uuid.toString()))
                .thenReturn(Mono.just(this.machineEntity));

        webTestClient.get().uri(ENDPOINT + "/{id}", uuid)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo(uuid.toString())
                .jsonPath("$.name").isEqualTo(INITIALIZED_NAME);

        Mockito.verify(reactiveRepository, times(1)).findById(uuid.toString());
    }

    @Test
    void deleteMachineTest() {

        UUID uuid = UUID.randomUUID();
        Mono<Void> voidReturn  = Mono.empty();
        Mockito.when(reactiveRepository.softDelete(uuid))
                .thenReturn(voidReturn);

        webTestClient.delete().uri(ENDPOINT + "/{id}", uuid)
                .exchange()
                .expectStatus().isOk();

        Mockito.verify(reactiveRepository, times(1)).softDelete(uuid);
    }

    @Test
    void hardDeleteMachineTest() {

        UUID uuid = UUID.randomUUID();
        Mono<Void> voidReturn  = Mono.empty();
        Mockito.when(reactiveRepository.deleteById(uuid))
                .thenReturn(voidReturn);

        webTestClient.delete().uri(ENDPOINT + "/{id}/hard-delete/{hardDeleteEnabled}", uuid, true)
                .exchange()
                .expectStatus().isOk();

        Mockito.verify(reactiveRepository, times(1)).deleteById(uuid);
    }

}
