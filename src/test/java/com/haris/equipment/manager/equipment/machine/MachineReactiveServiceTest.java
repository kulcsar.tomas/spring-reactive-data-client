package com.haris.equipment.manager.equipment.machine;

import com.haris.equipment.manager.equipments.machine.models.Machine;
import com.haris.equipment.manager.equipments.machine.models.MachineDTO;
import com.haris.equipment.manager.equipments.machine.MachineReactiveService;
import com.haris.equipment.manager.errorhandling.CustomException;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertNotNull;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@Import({MachineReactiveService.class, ModelMapper.class})
@DataR2dbcTest
@ActiveProfiles("test")
public class MachineReactiveServiceTest {

    private final MachineReactiveService machineReactiveService;

    private Machine machine;
    private static final String INITIALIZED_NAME = "MachineReactiveServiceTest";
    private static final String UPDATE_PREFIX = "UPDATED_";
    private static final String MACHINE_NOT_SAVED = "Machine could not be saved";

    @Autowired
    public MachineReactiveServiceTest(MachineReactiveService machineReactiveService) {
        this.machineReactiveService = machineReactiveService;
    }

    @BeforeEach
    public void createMachineDto() {
        this.machine = new Machine();
        this.machine.setName(INITIALIZED_NAME);
    }

    @AfterEach
    public void deconstruct() {
        this.machine = null;
    }

    @Test
    void fetchMachinesTest() throws CustomException {
        Machine machine2 = new Machine();
        machine2.setName(UPDATE_PREFIX + INITIALIZED_NAME);
        this.machineReactiveService.createMachine(this.machine).block();
        this.machineReactiveService.createMachine(machine2).block();
        StepVerifier.create(this.machineReactiveService.getMachines())
                .expectNextMatches(machine ->  machine2.getName().equals(machine.getName()))
                .expectNextMatches(machine ->  this.machine != null && this.machine.getName().equals(machine.getName()))
                .expectComplete()
                .verify();
    }

    @Test
    void fetchMachineByIdTest() throws CustomException {
        this.machine = this.machineReactiveService.createMachine(this.machine).block();
        assertNotNull(MACHINE_NOT_SAVED, this.machine);
        StepVerifier.create(this.machineReactiveService.getMachineById(this.machine.getIdString()))
                .expectNextMatches(machine -> this.machine.getId().equals(machine.getId()))
                .expectComplete()
                .verify();
    }

    /**
     * Throws an error when machineDto contains ID before creation
     */
    @Test
    void createMachineTestThrowsError() throws CustomException {
        this.machine.setId(UUID.randomUUID());
        StepVerifier.create(this.machineReactiveService.createMachine(this.machine))
                .expectError(CustomException.class).verify();
    }

    @Test
    void createMachineTest() throws CustomException {
        StepVerifier.create(this.machineReactiveService.createMachine(this.machine))
                .expectNext(this.machine)
                .expectComplete().verify();
    }

    /**
     * Throws an error when machineDto does not contains ID on update
     */
    @Test
    void updateMachineTestThrowsError() {
        StepVerifier.create(this.machineReactiveService.updateMachine(this.machine))
                .expectError(IllegalArgumentException.class).verify();
    }

    @Test
    void updateMachineTest() throws CustomException {
        this.machineReactiveService.createMachine(this.machine).block();
        assertNotNull(MACHINE_NOT_SAVED, this.machine);
        this.machine.setName(UPDATE_PREFIX + INITIALIZED_NAME);
        StepVerifier.create(this.machineReactiveService.updateMachine(this.machine))
                .expectNext(this.machine)
                .expectComplete().verify();
    }

    @Test
    void softDeleteTest() throws CustomException {
        this.machine = this.machineReactiveService.createMachine(this.machine).block();
        assertNotNull(MACHINE_NOT_SAVED, this.machine);
        this.machineReactiveService.deleteMachine(this.machine.getIdString(), Optional.of(false)).block();
        StepVerifier.create(this.machineReactiveService.getMachineById(this.machine.getIdString()))
                .expectNextMatches(machine -> machine.getDeletedAt() != null)
                .expectComplete().verify();
    }

    @Test
    void hardDeleteTest() throws CustomException {
        this.machine = this.machineReactiveService.createMachine(this.machine).block();
        assertNotNull(MACHINE_NOT_SAVED, this.machine);
        this.machineReactiveService.deleteMachine(this.machine.getIdString(), Optional.of(true)).block();
        StepVerifier.create(this.machineReactiveService.getMachineById(this.machine.getIdString()))
                .expectComplete().verify();
    }
}
